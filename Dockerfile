FROM golang:1.13-alpine3.10
ARG VERSION
ADD . /go/src/github.com/leominov/namespace-pool-admission-webhook
WORKDIR /go/src/github.com/leominov/namespace-pool-admission-webhook
RUN go build -o /bin/namespace-pool-admission-webhook -ldflags "-X main.Version=${VERSION}"

FROM alpine:3.10
COPY --from=0 /bin/namespace-pool-admission-webhook /bin
EXPOSE 8080
ENTRYPOINT ["/bin/namespace-pool-admission-webhook"]
