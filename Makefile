VERSION := $(strip $(shell cat ./VERSION))

.PHONY: build
build:
	@echo ">> building Docker image..."
	@docker build --build-arg "VERSION=$(VERSION)" --tag "eu.gcr.io/utilities-212509/devops/namespace-pool-webhook:$(VERSION)" ./

.PHONY: push
push:
	@echo ">> pushing Docker image..."
	@docker push "eu.gcr.io/utilities-212509/devops/namespace-pool-webhook:$(VERSION)"

.PHONY: test
test:
	@echo ">> running tests"
	@go test -short ./ --cover

.PHONY: test-report
test-report:
	@echo ">> generating report"
	@go test -coverprofile=coverage.txt && go tool cover -html=coverage.txt
