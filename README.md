# Kubernetes Namespace Pool Admission Webhook

[![Build Status](https://travis-ci.com/leominov/namespace-pool-admission-webhook.svg?branch=master)](https://travis-ci.com/leominov/namespace-pool-admission-webhook)

## Preparation

### Check support

```
$ kubectl api-versions | grep admissionregistration.k8s.io/v1beta1
```

### Create namespace

#### For Webhook

```
$ kubectl create namespace namespace-pool-webhook
```

#### For Applications

```
$ kubectl create namespace namespace-pool-example
$ kubectl label namespace namespace-pool-example namespace-pool=enabled
$ kubectl get namespace -L namespace-pool
```

### Generate a certificate signed by k8s CA

```
$ ./deployment/create-signed-cert.sh
$ kubectl get configmap -n kube-system extension-apiserver-authentication -o=jsonpath='{.data.client-ca-file}' | base64 | tr -d '\n'
```

Update `caBundle` in [deployment/namespace-pool-webhook.MutatingWebhookConfiguration.yaml](deployment/namespace-pool-webhook.MutatingWebhookConfiguration.yaml).

## Deploy

### Webhook

```
$ kubectl -n namespace-pool-webhook apply -f deployment/
$ kubectl -n namespace-pool-webhook get pods
```

### Application

```
$ kubectl -n namespace-pool-example apply -f application/
$ kubectl -n namespace-pool-example get pods -o json | jq .items[0].metadata.annotations
```

## Environment variables

* `LISTEN_ADDRESS` (`0.0.0.0:8443`)
* `TELEMETRY_LISTEN_ADDRESS` (`0.0.0.0:8080`)
* `TLS_CERT_FILE`
* `TLS_KEY_FILE`
* `FILTER_NAME`
* `NAMESPACE_POOL_MAP`
