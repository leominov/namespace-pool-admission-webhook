package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
)

var (
	// Version will be rewriten by CI
	Version string = "0.0.0-dev"

	logLevel = flag.String("log-level", "info", "Level of logging")
)

func main() {
	flag.Parse()

	logrus.SetFormatter(&logrus.JSONFormatter{})

	lvl, err := logrus.ParseLevel(*logLevel)
	if err != nil {
		logrus.Warn(err)
	} else {
		logrus.SetLevel(lvl)
	}

	opts, err := LoadOptions()
	if err != nil {
		logrus.Fatal(err)
	}
	s := NewServer(opts)

	logrus.Infof("Starting webhook %s on %v...", Version, opts.ListenAddress)
	logrus.Debugf("Configuration: %s", opts.String())

	go func() {
		err := s.ListenAndServe()
		if err != nil {
			logrus.Fatalf("Filed to listen and serve webhook server: %v", err)
		}
	}()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	<-signalChan

	logrus.Info("Got OS shutdown signal, shutting down webhook server gracefully...")
}
