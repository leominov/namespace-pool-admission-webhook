package main

import (
	"encoding/json"
	"fmt"
	"strings"

	corev1 "k8s.io/api/core/v1"
)

const (
	admissionWebhookAnnotationStatusKey = "namespace-pool-webhook.levaminov.ru/status"
)

// PatchOperation describe patch to update Kubernetes Specs
type PatchOperation struct {
	Op    string      `json:"op"`
	Path  string      `json:"path"`
	Value interface{} `json:"value,omitempty"`
}

// AddNodeSelectorTerms creates NodeSelector patch
func AddNodeSelectorTerms(key, value string) PatchOperation {
	matchExpressions := []corev1.NodeSelectorRequirement{
		corev1.NodeSelectorRequirement{
			Key:      key,
			Operator: corev1.NodeSelectorOpIn,
			Values:   []string{value},
		},
	}
	nodeSelectorTerms := []corev1.NodeSelectorTerm{
		corev1.NodeSelectorTerm{
			MatchExpressions: matchExpressions,
		},
	}
	nodeAffinity := &corev1.NodeAffinity{
		RequiredDuringSchedulingIgnoredDuringExecution: &corev1.NodeSelector{
			NodeSelectorTerms: nodeSelectorTerms,
		},
	}
	return PatchOperation{
		Op:   "add",
		Path: "/spec/affinity",
		Value: corev1.Affinity{
			NodeAffinity: nodeAffinity,
		},
	}
}

// UpdateTolerations creates Tolerations patch
func UpdateTolerations(tolerations []corev1.Toleration, key, value string) PatchOperation {
	var (
		exists bool
		index  int
	)
	for id, tol := range tolerations {
		if tol.Key == key {
			exists = true
			index = id
		}
	}
	patchValue := corev1.Toleration{
		Key:      key,
		Operator: corev1.TolerationOpEqual,
		Value:    value,
	}
	if exists {
		return PatchOperation{
			Op:    "replace",
			Path:  fmt.Sprintf("/spec/tolerations/%d", index),
			Value: patchValue,
		}
	}
	return PatchOperation{
		Op:    "add",
		Path:  "/spec/tolerations/-",
		Value: patchValue,
	}
}

// UpdateAnnotation creates Annotations patch
func UpdateAnnotation(annotations map[string]string, added map[string]string) []PatchOperation {
	patch := []PatchOperation{}
	for key, value := range added {
		if annotations == nil {
			patch = append(patch, PatchOperation{
				Op:   "add",
				Path: "/metadata/annotations",
				Value: map[string]string{
					key: value,
				},
			})
			continue
		}
		// ref: https://tools.ietf.org/html/rfc6901#section-3
		fixedName := strings.Replace(key, "/", "~1", -1)
		v, ok := annotations[key]
		if ok && v == value {
			continue
		} else if !ok {
			patch = append(patch, PatchOperation{
				Op:    "add",
				Path:  "/metadata/annotations/" + fixedName,
				Value: value,
			})
			continue
		}
		patch = append(patch, PatchOperation{
			Op:    "replace",
			Path:  "/metadata/annotations/" + fixedName,
			Value: value,
		})
	}
	return patch
}

// CreatePatch creates POD patch
func CreatePatch(pod *corev1.Pod, namespace string, annotations map[string]string) ([]byte, error) {
	var patch []PatchOperation
	patch = append(patch, UpdateAnnotation(pod.Annotations, annotations)...)
	patch = append(patch, UpdateTolerations(pod.Spec.Tolerations, namespace, "true"))
	patch = append(patch, AddNodeSelectorTerms(namespace, "true"))
	return json.Marshal(patch)
}
