package main

import (
	"reflect"
	"testing"

	corev1 "k8s.io/api/core/v1"
)

func TestUpdateAnnotation(t *testing.T) {
	tests := []struct {
		source, add map[string]string
		result      []PatchOperation
	}{
		{
			source: map[string]string{
				"foo": "bar",
			},
			add: map[string]string{
				"namespace-pool-webhook.levaminov.ru/status": "injected",
			},
			result: []PatchOperation{
				{
					Op:    "add",
					Path:  "/metadata/annotations/namespace-pool-webhook.levaminov.ru~1status",
					Value: "injected",
				},
			},
		},
		{
			source: map[string]string{
				"foobar/status": "yes",
			},
			add: map[string]string{
				"foobar/status": "no",
			},
			result: []PatchOperation{
				{
					Op:    "replace",
					Path:  "/metadata/annotations/foobar~1status",
					Value: "no",
				},
			},
		},
		{
			source: map[string]string{
				"foo": "bar",
			},
			add: map[string]string{
				"foo": "bar",
			},
			result: []PatchOperation{},
		},
		{
			source: nil,
			add: map[string]string{
				"bar": "foo",
			},
			result: []PatchOperation{
				{
					Op:   "add",
					Path: "/metadata/annotations",
					Value: map[string]string{
						"bar": "foo",
					},
				},
			},
		},
	}
	for i, test := range tests {
		result := UpdateAnnotation(test.source, test.add)
		if !reflect.DeepEqual(result, test.result) {
			t.Errorf("%d. Not the same", i)
		}
	}
}

func TestUpdateTolerations(t *testing.T) {
	tests := []struct {
		tolerations []corev1.Toleration
		key, value  string
		result      PatchOperation
	}{
		{
			tolerations: nil,
			key:         "foo",
			value:       "bar",
			result: PatchOperation{
				Op:   "add",
				Path: "/spec/tolerations/-",
				Value: corev1.Toleration{
					Key:      "foo",
					Operator: corev1.TolerationOpEqual,
					Value:    "bar",
				},
			},
		},
		{
			tolerations: []corev1.Toleration{
				{
					Key:      "foo",
					Operator: corev1.TolerationOpEqual,
					Value:    "bar",
				},
			},
			key:   "foo",
			value: "foo",
			result: PatchOperation{
				Op:   "replace",
				Path: "/spec/tolerations/0",
				Value: corev1.Toleration{
					Key:      "foo",
					Operator: corev1.TolerationOpEqual,
					Value:    "foo",
				},
			},
		},
	}
	for i, test := range tests {
		result := UpdateTolerations(test.tolerations, test.key, test.value)
		if !reflect.DeepEqual(result, test.result) {
			t.Errorf("%d. Not the same", i)
		}
	}
}

func TestCreatePatch(t *testing.T) {
	pod := &corev1.Pod{}
	pod.Annotations = nil
	pod.Spec.Tolerations = nil
	_, err := CreatePatch(pod, "default", nil)
	if err != nil {
		t.Error(err)
	}
}
