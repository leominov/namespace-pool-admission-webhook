package main

import (
	"encoding/json"
	"os"
	"regexp"
	"strings"
)

const (
	defaultListenAddress    = ":8443"
	defaultTelemetryAddress = ":8080"
)

type Options struct {
	ListenAddress          string `json:"listen_address"`
	TelemetryListenAddress string `json:"telemetry_listen_address"`
	TLSCertFile            string `json:"tls_cert_file"`
	TLSKeyFile             string `json:"tls_key_file"`
	FilterNameRaw          string `json:"filter_name"`
	NamespacePoolMapRaw    string `json:"namespace_pool_map"`
	filterName             *regexp.Regexp
	namespacePoolMap       map[string]string
}

func LoadOptions() (*Options, error) {
	o := &Options{
		ListenAddress:          os.Getenv("LISTEN_ADDRESS"),
		TelemetryListenAddress: os.Getenv("TELEMETRY_LISTEN_ADDRESS"),
		TLSCertFile:            os.Getenv("TLS_CERT_FILE"),
		TLSKeyFile:             os.Getenv("TLS_KEY_FILE"),
		FilterNameRaw:          os.Getenv("FILTER_NAME"),
		NamespacePoolMapRaw:    os.Getenv("NAMESPACE_POOL_MAP"),
		namespacePoolMap:       make(map[string]string),
	}
	o.FillWithDefaults()
	if err := o.Parse(); err != nil {
		return nil, err
	}
	return o, nil
}

func (o *Options) FillWithDefaults() {
	if len(o.ListenAddress) == 0 {
		o.ListenAddress = defaultListenAddress
	}
	if len(o.TelemetryListenAddress) == 0 {
		o.TelemetryListenAddress = defaultTelemetryAddress
	}
}

func (o *Options) Parse() error {
	if len(o.FilterNameRaw) > 0 {
		re, err := regexp.Compile(o.FilterNameRaw)
		if err != nil {
			return err
		}
		o.filterName = re
	}
	if len(o.NamespacePoolMapRaw) == 0 {
		return nil
	}
	items := strings.Split(o.NamespacePoolMapRaw, ",")
	for _, pair := range items {
		pair = strings.TrimSpace(pair)
		kv := strings.Split(pair, "=")
		if len(kv) != 2 {
			continue
		}
		o.namespacePoolMap[kv[0]] = kv[1]
	}
	return nil
}

func (o *Options) String() string {
	b, _ := json.Marshal(o)
	return string(b)
}
