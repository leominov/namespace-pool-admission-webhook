package main

import (
	"os"
	"testing"
)

func unsetEnvOptions() {
	os.Unsetenv("LISTEN_ADDRESS")
	os.Unsetenv("TELEMETRY_LISTEN_ADDRESS")
	os.Unsetenv("TLS_CERT_FILE")
	os.Unsetenv("TLS_KEY_FILE")
	os.Unsetenv("FILTER_NAME")
	os.Unsetenv("NAMESPACE_POOL_MAP")
}

func TestLoadOptions(t *testing.T) {
	unsetEnvOptions()
	options, err := LoadOptions()
	if err != nil {
		t.Error(err)
	}
	if options.ListenAddress != defaultListenAddress {
		t.Errorf("Must be %s, but got %s", defaultListenAddress, options.ListenAddress)
	}
	if options.TelemetryListenAddress != defaultTelemetryAddress {
		t.Errorf("Must be %s, but got %s", defaultTelemetryAddress, options.TelemetryListenAddress)
	}
	unsetEnvOptions()
	os.Setenv("LISTEN_ADDRESS", ":1234")
	os.Setenv("TELEMETRY_LISTEN_ADDRESS", ":5678")
	os.Setenv("FILTER_NAME", "foo-bar")
	os.Setenv("NAMESPACE_POOL_MAP", "foo=bar, bar=foo,empty")
	options, err = LoadOptions()
	if err != nil {
		t.Error(err)
	}
	if options.ListenAddress != ":1234" {
		t.Error(options.ListenAddress)
	}
	if options.TelemetryListenAddress != ":5678" {
		t.Error(options.TelemetryListenAddress)
	}
	unsetEnvOptions()
	os.Setenv("FILTER_NAME", "@#$@#$ER$%&((")
	_, err = LoadOptions()
	if err == nil {
		t.Error(err)
	}
}

func TestString_Options(t *testing.T) {
	unsetEnvOptions()
	options, err := LoadOptions()
	if err != nil {
		t.Error(err)
	}
	if len(options.String()) == 0 {
		t.Error("Empty string")
	}
}
