package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"k8s.io/api/admission/v1beta1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
)

var (
	runtimeScheme = runtime.NewScheme()
	codecs        = serializer.NewCodecFactory(runtimeScheme)
	deserializer  = codecs.UniversalDeserializer()
)

type Server struct {
	o                    *Options
	mutationsTotalMetric *prometheus.GaugeVec
	errorsTotalMetric    *prometheus.GaugeVec
}

func NewServer(o *Options) *Server {
	server := &Server{
		o: o,
		mutationsTotalMetric: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: "namespace_pool_webhook",
			Name:      "mutations_total",
			Help:      "Number of successed mutations.",
		}, []string{"namespace"}),
		errorsTotalMetric: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: "namespace_pool_webhook",
			Name:      "errors_total",
			Help:      "Number of failed mutations.",
		}, []string{"namespace"}),
	}

	// Register own metrics
	prometheus.MustRegister(server.mutationsTotalMetric)
	prometheus.MustRegister(server.errorsTotalMetric)

	return server
}

func (s *Server) MutateHandler(w http.ResponseWriter, r *http.Request) {
	var (
		body              []byte
		admissionResponse *v1beta1.AdmissionResponse
	)
	logrus.Debug("Got MutateHandler request")
	if r.Body != nil {
		if data, err := ioutil.ReadAll(r.Body); err == nil {
			body = data
		}
	}
	if len(body) == 0 {
		s.errorsTotalMetric.WithLabelValues("").Inc()
		logrus.Error("Error reading request body")
		http.Error(w, "Error reading request body", http.StatusBadRequest)
		return
	}
	contentType := r.Header.Get("Content-Type")
	if contentType != "application/json" {
		s.errorsTotalMetric.WithLabelValues("").Inc()
		logrus.Errorf("Unsupporter Content-Type: %s", contentType)
		http.Error(w, fmt.Sprintf("Unsupporter Content-Type: %s", contentType), http.StatusUnsupportedMediaType)
		return
	}

	ar := v1beta1.AdmissionReview{}
	if _, _, err := deserializer.Decode(body, nil, &ar); err != nil {
		s.errorsTotalMetric.WithLabelValues("").Inc()
		logrus.Errorf("Can't decode request body: %v", err)
		admissionResponse.Result = &metav1.Status{
			Message: err.Error(),
		}
	} else {
		admissionResponse = s.Mutate(&ar)
	}

	admissionReview := v1beta1.AdmissionReview{}
	if admissionResponse != nil {
		admissionReview.Response = admissionResponse
		if ar.Request != nil {
			admissionReview.Response.UID = ar.Request.UID
		}
	}

	encoder := json.NewEncoder(w)
	if err := encoder.Encode(admissionReview); err != nil {
		s.errorsTotalMetric.WithLabelValues("").Inc()
		logrus.Errorf("Can't encode response body: %v", err)
		http.Error(w, "Can't encode response body", http.StatusInternalServerError)
	}
}

func (s *Server) HealthyHandler(w http.ResponseWriter, r *http.Request) {
	if _, err := w.Write([]byte(`ok`)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s *Server) serveMetrics() error {
	logrus.Infof("Serving telemetry on %s...", s.o.TelemetryListenAddress)

	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	mux.HandleFunc("/healthy", s.HealthyHandler)
	return http.ListenAndServe(s.o.TelemetryListenAddress, mux)
}

func (s *Server) ListenAndServe() error {
	go func() {
		if err := s.serveMetrics(); err != nil {
			logrus.Fatalf("error serving telemetry: %s", err)
		}
	}()

	http.HandleFunc("/mutate", s.MutateHandler)
	http.HandleFunc("/healthy", s.HealthyHandler)

	if len(s.o.TLSCertFile) == 0 || len(s.o.TLSKeyFile) == 0 {
		logrus.Warn("Certificate disabled")
		return http.ListenAndServe(s.o.ListenAddress, nil)
	}

	return http.ListenAndServeTLS(s.o.ListenAddress, s.o.TLSCertFile, s.o.TLSKeyFile, nil)
}

func (s *Server) Mutate(ar *v1beta1.AdmissionReview) *v1beta1.AdmissionResponse {
	var pod corev1.Pod
	admissionResponse := &v1beta1.AdmissionResponse{}
	req := ar.Request
	if err := json.Unmarshal(req.Object.Raw, &pod); err != nil {
		s.errorsTotalMetric.WithLabelValues("").Inc()
		admissionResponse.Result = &metav1.Status{
			Message: err.Error(),
		}
		return admissionResponse
	}

	poolName := req.Namespace
	if pool, ok := s.o.namespacePoolMap[req.Namespace]; ok {
		poolName = pool
	}

	if s.o.filterName != nil && !s.o.filterName.MatchString(pod.GenerateName) {
		logrus.Debugf(
			"AdmissionReview for Kind=%v, Namespace=%v Name=%v Pool=%v is disabled by name filter",
			req.Kind, req.Namespace, pod.GenerateName, poolName,
		)
		admissionResponse.Allowed = true
		return admissionResponse
	}

	logrus.Infof(
		"AdmissionReview for Kind=%v, Namespace=%v Name=%v Pool=%v UID=%v PatchOperation=%v UserInfo=%v",
		req.Kind, req.Namespace, pod.GenerateName, req.UID, poolName, req.Operation, req.UserInfo,
	)

	annotations := map[string]string{
		admissionWebhookAnnotationStatusKey: "injected",
	}
	patchBytes, err := CreatePatch(&pod, poolName, annotations)
	if err != nil {
		s.errorsTotalMetric.WithLabelValues(req.Namespace).Inc()
		admissionResponse.Result = &metav1.Status{
			Message: err.Error(),
		}
		return admissionResponse
	}

	s.mutationsTotalMetric.WithLabelValues(req.Namespace).Inc()
	logrus.Infof("AdmissionResponse: patch=%v", string(patchBytes))
	admissionResponse = &v1beta1.AdmissionResponse{
		Allowed: true,
		Patch:   patchBytes,
		PatchType: func() *v1beta1.PatchType {
			pt := v1beta1.PatchTypeJSONPatch
			return &pt
		}(),
	}

	return admissionResponse
}
