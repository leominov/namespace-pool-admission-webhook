package main

import "testing"

func TestNewServer(t *testing.T) {
	server := NewServer(&Options{})
	if server == nil {
		t.Error("NewServer() = nil")
	}
}
